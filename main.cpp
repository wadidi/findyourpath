#include "mainwindow.h"
#include <QApplication>
#include <QList>
#include <QVariant>
#include "station.h"
#include "qeditablevalue.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
