#include "stationlistmodel.h"
#include "station.h"
#include "qeditablevalue.h"

StationListModel::StationListModel(const QList<Station> & listeStations, QObject* parent) : QAbstractListModel(parent), listeStations(listeStations){}

int StationListModel::rowCount(const QModelIndex &parent) const{
    return this->listeStations.count();
}

QVariant StationListModel::data(const QModelIndex &index, int role) const{
    if (! index.isValid())
        return QVariant();
    if (index.row()>=this->listeStations.count())
        return QVariant();
    if (role == Qt::DisplayRole){
        return this->listeStations.at(index.row()).getNom(); //Retourne le nom de la station demandé
    }
    else return QVariant();
}

int StationListModel::getNbStation() const{
    return listeStations.count();
}
