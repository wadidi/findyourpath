#ifndef STATIONMODEL_H
#define STATIONMODEL_H

/*!
 * \file station.h
 * \brief Conteneur de station
 * \author Lucas Miallet & Mehdi Khadir
 */

#include<QString>
#include<QPainter>
#include<QRect>
#include<QPalette>
#include<QSize>

/*! \class Station
   * \brief classe representant une station
   *
   *  La classe gere les elements representants une sation
   */

class Station
{
    /*!
         *  \brief Ecriture dans un stream
         *
         * Methode qui permet d'ecrire un objet custom dans un qdatastream
         * Necessaire lors de l'implementation de l'ecriture d'un objet custom dans les qsettings
         *
         *  \param arch : reference sur le qdatastream ou ecrire
         *  \param object : reference sur l'objet a ecrire
         *  \return le qdatastream avec l'objet ecrit a l'interieur
         */
    friend QDataStream & operator << (QDataStream &arch, const Station & object)
    {
        arch << object.nom;
        return arch;
    }

    /*!
         *  \brief Ecriture dans l'objet donne
         *
         * Methode qui permet d'ecrire d'un qdatastream dans un objet donne
         * Necessaire lors de l'implementation de la lecture d'un objet custom dans les qsettings
         *
         *  \param arch : reference sur le qdatastream ou lire
         *  \param object : reference sur l'objet a remplir
         *  \return le qdatastream
         */
    friend QDataStream & operator >> (QDataStream &arch, Station & object)
    {
        arch >> object.nom;
        return arch;
    }

public:
    enum EditMode { Editable, ReadOnly };

    /*!
         *  \brief Constructeur
         *
         *  Constructeur par defaut de la classe Station
         *
         */
    Station();

    /*!
         *  \brief Constructeur avec valeur
         *
         *  Constructeur avec valeur donnee de la classe Station
         *
         *  \param nom : nom de la station
         */
    Station(const QString & nom);

    /*!
         *  \brief Getter
         *
         *  Getter du nom de la station
         *
         *  \return le nom de la station
         */
    QString getNom() const;

    /*!
         *  \brief Setter
         *
         *  Setter du nom de la station
         *
         *  \param nom : le nom de la station
         */
    void setNom(const QString & nom);

    /*!
         *  \brief Surcharge d'operateur ==
         *
         *  Surcharge de l'operateur ==
         *  permettant de 2 stations entre elles
         *
         *  \param s : la station a comparer
         */
    bool operator==(const Station & s);

private :
    QString nom; /*!< Nom de la station*/

};

Q_DECLARE_METATYPE(Station)

#endif // STATIONMODEL_H
