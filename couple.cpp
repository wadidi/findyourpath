#include "couple.h"
#include "ligne.h"

Couple::Couple(const Ligne & ligne) : ligne(ligne){}

Station Couple::getDepart() const{
    return depart;
}

void Couple::setDepart(const Station & depart){
    this->depart = depart;
}

Station Couple::getArrivee() const{
    return arrivee;
}

void Couple::setArrivee(const Station & arrivee){
    this->arrivee = arrivee;
}

Ligne Couple::getLigne() const {
    return this->ligne;
}

void Couple::setLigne(const Ligne & ligne){
    this->ligne = ligne;
}
