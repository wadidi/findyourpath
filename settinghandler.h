#ifndef SETTINGHANDLER_H
#define SETTINGHANDLER_H

/*!
 * \file settinghandler.h
 * \brief Prise en charge de l'enregistrement des données dans les qsettings
 * \author Lucas Miallet & Mehdi Khadir
 */

#include <QList>
#include <QSettings>

#include "trajet.h"

/*! \class SettingHandler
   * \brief Singleton représentant un instance de qsettings
   *
   * La classe gère l'enregistrement et la récupération des données
   * provenant des qsettings
   */

class SettingHandler
{
public:

    /*!
         *  \brief Recuperation du Singleton
         *
         *  Construction et recuperation du singleton s'il existe deja
         *
         *  \return un pointeur sur l'instance du singleton
         */
    static SettingHandler* sharedInstance();

    /*!
         *  \brief Setter
         *
         * Enregistrement d'un nouveau trajet dans la liste de tous les trajets deja presents s'il n'y existe pas deja
         * mise a jour du trajet correspondant s'il existe deja
         *
         * \param monTrajet : trajet a enregistrer ou a mettre a jour
         * \param isSaved : indique si c'est une mise a jour ou un enregistrement
         * \param row : index du trajet dans la liste
         */
    void saveTrajet(const Trajet & monTrajet, bool isSaved, int row);

    /*!
         *  \brief Getter
         *
         *  Recuperation de tous les trajets presents dans qsetting
         *
         *  \return la liste des trajets enregistree par l'utilisateur
         */
    const QList<Trajet> getAllTrajet();

    void deleteAll();

    /*!
         *  \brief Suppression d'un trajet
         *
         *  Supprime le trajet a la ligne row des qsettings
         */

    void deleteTrajet(int row);

private:

    /*!
         *  \brief Constructeur
         *
         *  Constructeur prive de la classe SettingHandler
         *
         */
    SettingHandler();

    static SettingHandler* settingSharedInstance; /*!< Singleton*/
    QList<Trajet> trajets; /*!< Liste des trajets enregistres*/
    QSettings settingsH; /*!< Setting contenant une referance sur QSetting*/
};

#endif // SETTINGHANDLER_H
