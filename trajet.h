#ifndef TRAJETMODEL_H
#define TRAJETMODEL_H

/*!
 * \file trajet.h
 * \brief Conteneur de trajet
 * \author Lucas Miallet & Mehdi Khadir
 */

#include <QList>
#include <QString>
#include "couple.h"

/*! \class Trajet
   * \brief classe representant un trajet
   *
   *  La classe gere les elements representants un trajet
   */

class Trajet
{

    /*!
         *  \brief Ecriture dans un stream
         *
         * Methode qui permet d'ecrire un objet custom dans un qdatastream
         * Necessaire lors de l'implementation de l'ecriture d'un objet custom dans les qsettings
         *
         *  \param arch : reference sur le qdatastream ou ecrire
         *  \param object : reference sur l'objet a ecrire
         *  \return le qdatastream avec l'objet ecrit a l'interieur
         */
    friend QDataStream & operator << (QDataStream &arch, const Trajet & object)
        {
            arch << object.nom;
            arch << object.trajet;
            arch << object.comments;
            return arch;
        }

    /*!
         *  \brief Ecriture dans l'objet donne
         *
         * Methode qui permet d'ecrire d'un qdatastream dans un objet donne
         * Necessaire lors de l'implementation de la lecture d'un objet custom dans les qsettings
         *
         *  \param arch : reference sur le qdatastream ou lire
         *  \param object : reference sur l'objet a remplir
         *  \return le qdatastream
         */
    friend QDataStream & operator >> (QDataStream &arch, Trajet & object)
    {
        arch >> object.nom;
        arch >> object.trajet;
        arch >> object.comments;
        return arch;
    }

public:
    /*!
         *  \brief Constructeur
         *
         *  Constructeur par defaut de la classe Trajet
         *
         */
    Trajet() = default;

    /*!
         *  \brief Constructeur avec valeur
         *
         *  Constructeur avec valeur donnee de la classe Trajet
         *
         *  \param nom : nom du trajet
         */
    Trajet(const QString & nom);

    /*!
         *  \brief Constructeur de copie
         *
         *  Constructeur avec de copie de la classe Trajet
         *
         *  \param trajet : le trajet a copier
         */
    Trajet(const Trajet & t);

    /*!
         *  \brief Getter
         *
         *  Getter du nom du trajet
         *
         *  \return le nom du trajet
         */
    QString getNom() const;

    /*!
         *  \brief Setter
         *
         *  Setter du nom du trajet
         *
         *  \param name : le nom du trajet
         */
    void setNom(QString name);

    /*!
         *  \brief Getter
         *
         *  Getter de la liste de tous les couples
         *
         *  \return liste de tous les couples
         */
    const QList<Couple> getCouple() const;

    /*!
         *  \brief Setter
         *
         *  Ajout d'un nouveau couple a la liste de trajet
         *
         *  \param couple : le couple a ajouter
         */
    void addCouple(Couple& couple);

    /*!
         *  \brief ToString
         *
         *  Methode d'affichage d'un trajet
         *
         *  \return une string representant le trajet
         */
    QString toString();

    /*!
         *  \brief Reset
         *
         *  Remets à zéro la liste de trajets et le nom du trajet
         */
    void reset();

    /*!
         *  \brief Suppression du dernier couple
         *
         *  Retire le dernier couple du trajet
         */

    void removeLastCouple();

    /*!
         *  \brief Setter
         *
         *  Modification d'un couple dans la liste
         *
         *  \param depart : station de depart a modifier
         *  \param arrivee : station d'arrivee a modifier
         *  \param index : index du couple a modifier
         */
    void setCouple(Station depart, Station arrivee, int index);

    /*!
         *  \brief Getter
         *
         *  Getter de la taille du trajet
         *
         *  \return taille de la liste de couple de trajet
         */
    int size();

    /*!
         *  \brief Setter
         *
         *  Modification du commentaire du trajet
         *
         *  \param comments : commentaires a definir
         */

    void setComment(QString comments);

    /*!
         *  \brief Getter
         *
         *  Getter des commentaires sur le trajet
         *
         *  \return les commentaires sur le trajet
         */

    QString getComment() const;

private:
    QString nom; /*!< Nom du trajet */
    QList<Couple> trajet; /*!< Liste des trajets*/
    QString comments;
};

Q_DECLARE_METATYPE(Trajet)

#endif // TRAJETMODEL_H
