#include "lignelistmodel.h"

int GET_LIGNE = 123;

LigneListModel::LigneListModel(const QList<Ligne> & listeLignes, QObject * parent) : QAbstractListModel(parent), listeLignes(listeLignes){
}

int LigneListModel::rowCount(const QModelIndex &parent) const{
    return this->listeLignes.count();
}

QVariant LigneListModel::data(const QModelIndex &index, int role) const{
    if (! index.isValid())
        return QVariant(); //Returns an invalid QVariant if there is no value to return
    if (index.row()>=this->listeLignes.count())
        return QVariant(); //Returns an invalid QVariant if there is no value to return
    if (role == Qt::DecorationRole){
        return this->listeLignes.at(index.row()).getIcon();
    }

    if(role == LigneListModel::GET_LIGNE) {
        return QVariant::fromValue(this->listeLignes.at(index.row()));
    }

    else return QVariant();

}

