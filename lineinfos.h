#ifndef LINEINFOS_H
#define LINEINFOS_H

/*!
 * \file lineinfos.h
 * \brief Associations couleur - ligne - icone
 * \author Lucas Miallet & Mehdi Khadir
 */


#include <QColor>
#include <QList>
#include "ligne.h"

/*! \class LineInfos
 * \brief Associations couleur - ligne - icone
 */

class LineInfos
{
public:

    /*!
         *  \brief Constructeur
         *
         *  Constructeur par defaut de la classe LineInfos
         */

    LineInfos();

    /*!
         *  \brief Getter de la station à l'index precis
         *  \param index : index de la station souhaitee
         *  \return La station trouvee
         */

    Ligne getStation(int index);

    /*!
         *  \brief Getter de la liste de stations
         *  \return La liste des stations avec leur nom, couleur et icone
         */

    QList<Ligne> getList();
private:
    QList<Ligne> lignesList; /*!< Liste stockant l'ensemble des informations relatives aux lignes*/

};

#endif // LINEINFOS_H
