#ifndef TRAJETLISTMODEL_H
#define TRAJETLISTMODEL_H

/*!
 * \file trajetlistmodel.h
 * \brief Model de liste de trajets
 * \author Lucas Miallet & Mehdi Khadir
 */

#include <QString>
#include <QList>
#include <QAbstractListModel>
#include <QVariant>
#include <QObject>
#include <QModelIndex>

#include "trajet.h"

/*! \class TrajetListModel
   * \brief model de liste du trajet
   *
   *  La classe gere l'affichage des trajets dans la liste
   */

class TrajetListModel: public QAbstractListModel
{

public:
    const static int GET_TRAJET = 25;

    /*!
         *  \brief Constructeur
         *
         *  Constructeur de la classe TrajetListModel
         *
         *  \param listeTrajets : liste de trajets a afficher
         *  \param parent : parent du model
         */
    TrajetListModel(const QList<Trajet> & listeTrajets, QObject * parent = 0);

    /*!
         *  \brief Compteur du nombre de lignes
         *
         *  Returns the number of rows under the given parent.
         *  When the parent is valid it means that rowCount is returning the number of children of parent.
         *
         *  \param parent : parent du model
         *  \return le nombre d'enfant du parent
         */
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    /*!
         *  \brief Recuperation des donnees
         *
         *  Returns the data stored under the given role for the item referred to by the index.
         *
         *  \param index : index du model
         *  \param role : role a recuperer
         *  \return donnee enregistree en fonction du bon rôle
         */
    QVariant data(const QModelIndex &index, int role) const;

    /*!
         *  \brief Getter
         *
         *  Getter du nombre de trajet
         *
         *  \return le nombre de trajets
         */
    int getNbTrajet() const;

    /*!
         *  \brief Ajout d'un nouveau trajet
         *
         *  Ajout d'un nouveau trajet dans la liste actuelle
         *
         *  \param t : le trajet a inserer
         *  \param row : la ligne ou inserer le trajet
         */
    void insertNewTrajet(Trajet* t, int row);

    /*!
         *  \brief Suppression d'un trajet
         *
         *  Suppression du trajet à la ligne row
         *
         *  \param row : la ligne ou inserer le trajet
         */

    void deleteTrajet(int row);

    /*!
         *  \brief Setter de name
         *
         *  Change le nom du trajet a la ligne donnee
         *
         *  \param name : le nom a modifier
         *  \param row : la ligne ou modifier le nom
         */

    void changeName(QString name, int row);

    /*!
         *  \brief Setter de commentaire
         *
         *  Change le commentaire du trajet a la ligne donnee
         *
         *  \param comment : le commentaire a modifier
         *  \param row : la ligne ou modifier le nom
         */
    void changeComment(QString comment, int row);

    void clear();

    void updateRow(Trajet t, int row);

private :
    QList<Trajet> listeTrajets; /*!< Liste des trajets*/
};

#endif // TRAJETLISTMODEL_H
