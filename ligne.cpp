#include "ligne.h"

Ligne::Ligne(QColor color, QString nom) : color(color), nom(nom){}

Ligne::Ligne(QColor color, QString nom, QIcon icon) : color(color), nom(nom), icon(icon){}

QColor Ligne::getColor() const{
    return color;
}

QString Ligne::getNom() const{
    return nom;
}

void Ligne::addStation(const Station & station){
    if(!stations.contains(station)) //Add if not already in
        stations.push_back(station);
}

Station Ligne::removeStation(const QString & nom){
    Station station(nom); //Station provisoire
    stations.removeOne(station);
    return station;
}

QIcon Ligne::getIcon() const{
    return icon;
}

