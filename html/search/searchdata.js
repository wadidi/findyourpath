var indexSectionsWithContent =
{
  0: "abcdeghilmnopqrstuv~",
  1: "clmpqrst",
  2: "u",
  3: "clmpqrst",
  4: "acdgilmopqrstv~",
  5: "abcdghilnprstu",
  6: "esv",
  7: "acdeilprst",
  8: "itv",
  9: "o",
  10: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "related",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Friends",
  10: "Pages"
};

