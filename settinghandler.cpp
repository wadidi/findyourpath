#include "settinghandler.h"

#include <QVariant>
#include <QCoreApplication>
#include <QDebug>

SettingHandler* SettingHandler::settingSharedInstance = nullptr;

SettingHandler::SettingHandler() : trajets(), settingsH("FindYourPath", "FindYourPath1") {}

SettingHandler* SettingHandler::sharedInstance() {
    if(settingSharedInstance == nullptr) {
        settingSharedInstance = new SettingHandler();
    }
    return settingSharedInstance;
}

void SettingHandler::saveTrajet(const Trajet & monTrajet, bool isSaved, int row) {
    qDebug() << "is saved ? " << isSaved;
    qDebug() << " Commentaire cote saveTrajet : " << monTrajet.getComment();
    if(!isSaved) {
        this->trajets.insert(0,monTrajet);
    } else {
        if(this->trajets.size() > 0) {
            this->trajets[row] = monTrajet;
        } else {
            this->trajets.insert(0,monTrajet);
        }
    }
    qRegisterMetaTypeStreamOperators<QList<Trajet>>("mesTrajets");
    qRegisterMetaTypeStreamOperators<Trajet>("Trajet");
    this->settingsH.setValue("mesTrajets", QVariant::fromValue(this->trajets));
}

const QList<Trajet> SettingHandler::getAllTrajet() {
    qRegisterMetaTypeStreamOperators<QList<Trajet>>("mesTrajets");
    qRegisterMetaTypeStreamOperators<Trajet>("Trajet");
    this->trajets = settingsH.value("mesTrajets").value<QList<Trajet>>();
    return this->trajets;
}

void SettingHandler::deleteAll() {
    this->trajets.clear();
    qRegisterMetaTypeStreamOperators<QList<Trajet>>("mesTrajets");
    qRegisterMetaTypeStreamOperators<Trajet>("Trajet");
    this->settingsH.setValue("mesTrajets", QVariant::fromValue(this->trajets));
}

void SettingHandler::deleteTrajet(int row){
    this->trajets.removeAt(row);
    qRegisterMetaTypeStreamOperators<QList<Trajet>>("mesTrajets");
    qRegisterMetaTypeStreamOperators<Trajet>("Trajet");
    this->settingsH.setValue("mesTrajets", QVariant::fromValue(this->trajets));
}
