#ifndef STATIONLISTMODEL_H
#define STATIONLISTMODEL_H

#include <QString>
#include <QList>
#include <QAbstractListModel>
#include <QVariant>
#include <QObject>
#include <QModelIndex>
#include "qeditablevalue.h"
#include "station.h"



class StationListModel : public QAbstractListModel
{
public:
    StationListModel(const QList<Station> & listeStations, QObject * parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;

    int getNbStation() const;

private:
    QList<Station> listeStations;
};

#endif // STATIONLISTMODEL_H
