#ifndef POINTDRAWER_H
#define POINTDRAWER_H

/*!
 * \file pointdrawer.h
 * \brief Dessinateur de point
 * \author Lucas Miallet & Mehdi Khadir
 */

#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QFrame>

/*! \class PointDrawer
   * \brief classe representant le point dessiné
   *
   *  La classe gere le dessin d'un point de couleur definie, representant un depart
   */

class PointDrawer : public QWidget
{
    Q_OBJECT

public:
    enum Shape { Line, Points, Polyline, Polygon, Rect, RoundedRect, Ellipse, Arc,
                 Chord, Pie, Path, Text, Pixmap };

    /*!
         *  \brief Constructeur
         *
         *  Constructeur de la classe PointDrawer
         *
         *  \param color : couleur du point
         *  \param parent : parent du widget
         */

    PointDrawer(const QColor & color, QWidget *parent = 0);

    /*!
         *  \brief Ajout d'un morceau
         *
         *  Methode qui permet d'ajouter un morceau a liste de
         *  lecture
         *
         *  \param strSong : le morceau a ajouter
         *  \return true si morceau deja present dans la liste,
         *  false sinon
         */
    /*!
         *  \brief Getter minimumSizeHint
         *  \return Le minimumSizeHint du QWidget
         */
    QSize minimumSizeHint() const override;
    /*!
         *  \brief Getter sizeHint
         *  \return Lea sizeHint du QWidget
         */
    QSize sizeHint() const override;

public slots:
    /*!
         *  \brief Definition de la forme
         *
         *  Sets the cursor to the shape identified by shape.
         *
         *  \param shape : forme a associer au curseur
         */
    void setShape(Shape shape);
    /*!
         *  \brief Definition du pinceau
         *
         *  Sets the painter's pen to be the given pen.
         *
         *  \param pen : pinceau a associer
         */
    void setPen(const QPen &pen);
    /*!
         *  \brief Definition de la brosse
         *
         *  Sets the painter's brush to the given brush.
         *
         *  \param brush : brosse a associer
         */
    void setBrush(const QBrush &brush);
    /*!
         *  \brief Definition du lissage
         *
         *  If enable is true, antialiased painting is enabled.
         *
         *  \param shape : booleen valant true si le lissage est a activer et false sinon
         */
    void setAntialiased(bool antialiased);
    /*!
         *  \brief Definition de la transformation
         *
         *  If the given parameter transformed is true, transformed is set to true
         *
         *  \param transformed : booleen valant true si la transformation est a activer et false sinon
         */
    void setTransformed(bool transformed);

protected:
    /*!
         *  \brief Redéfinition du paintEvent
         *
         *  \param paintEvent : Evenement a passer au painter
         */
    void paintEvent(QPaintEvent *event) override;

private:
    Shape shape; /*!< Forme du pinceau*/
    QPen pen; /*!< Pinceau utilise*/
    QBrush brush; /*!< Brosse utilisee*/
    bool antialiased; /*!< Lissage*/
    bool transformed; /*!< Transformation*/
    QPixmap pixmap; /*!< pixmap utilise*/
    QColor color; /*!< Couleur du point*/
};

#endif // POINTDRAWER_H
