#include <QDebug>

#include "trajetlistmodel.h"
#include "trajet.h"

int GET_TRAJET = 25;

TrajetListModel::TrajetListModel(const QList<Trajet> & listeTrajets, QObject * parent) : QAbstractListModel(parent), listeTrajets(listeTrajets){}

int TrajetListModel::rowCount(const QModelIndex &parent) const{
    return this->listeTrajets.count();
}

QVariant TrajetListModel::data(const QModelIndex &index, int role) const{
    if (! index.isValid())
        return QVariant();
    if (index.row()>=this->listeTrajets.count())
        return QVariant();
    if (role == Qt::DisplayRole) {
        return this->listeTrajets.at(index.row()).getNom(); // retourne le nom du trajet demandé
    }

    if(role == TrajetListModel::GET_TRAJET) {
        return QVariant::fromValue(this->listeTrajets.at(index.row()));
    }

    else return QVariant();

}

int TrajetListModel::getNbTrajet() const {
    return listeTrajets.count();
}

void TrajetListModel::insertNewTrajet(Trajet* t, int row = -1) {
    int rowp = row < 0 ? this->listeTrajets.size() : row;
    beginInsertRows(QModelIndex(), rowp, rowp);
    this->listeTrajets.insert(row, *t);
    endInsertRows();
}

void TrajetListModel::deleteTrajet(int row = -1){
    int rowp = row < 0 ? this->listeTrajets.size() : row;
    beginRemoveRows(QModelIndex(), rowp, rowp);
    this->listeTrajets.removeAt(row);
    endRemoveRows();
}

void TrajetListModel::changeName(QString name, int row) {
    int rowp = row < 0 || row > this->listeTrajets.size() ? this->listeTrajets.size() : row;
    this->listeTrajets[rowp].setNom(name);
    dataChanged(QModelIndex(), QModelIndex());
}

void TrajetListModel::changeComment(QString comment, int row) {
    int rowp = row < 0 || row > this->listeTrajets.size() ? this->listeTrajets.size() : row;
    this->listeTrajets[rowp].setComment(comment);
    dataChanged(QModelIndex(), QModelIndex());
}

void TrajetListModel::clear() {
    beginRemoveRows(QModelIndex(), 0, this->listeTrajets.size());
    this->listeTrajets.clear();
    endRemoveRows();
}

void TrajetListModel::updateRow(Trajet t, int row) {
    if(row >= 0 && row <= this->listeTrajets.size()) {
        this->listeTrajets[row] = t;
    }
}
