#ifndef COUPLEMODEL_H
#define COUPLEMODEL_H

/*!
 * \file couple.h
 * \brief Couple de stations depart <-> arrivee
 * \author Lucas Miallet & Mehdi Khadir
 */

#include "ligne.h"
#include "station.h"
#include <QString>

  /*! \class Couple
   * \brief Couple de stations depart <-> arrivee
   *
   *  Contient deux stations, une station de depart et une d'arrivee, correspondant a un bout du trajet entre 2 correspondances
   */

class Couple
{
    /*!
         *  \brief Ecriture sur un flux
         *
         *  Methode qui permet d'ecrire sur le flux arch la ligne du couple, la station de depart et celle d'arrivee
         *
         *  \param arch : flux sur lequel ecrire
         *  \param object : couple a envoyer sur le flux arch
         */
    friend QDataStream & operator << (QDataStream &arch, const Couple & object)
    {
        arch << object.ligne;
        arch << object.depart;
        arch << object.arrivee;
        return arch;
    }

    /*!
         *  \brief Lecture a partir d'un flux
         *
         *  Methode qui permet de lire a partir flux arch la ligne du couple, la station de depart et celle d'arrivee
         *
         *  \param arch : flux a partir duquel lire
         *  \param object : couple sur lequel ecrire
         */

    friend QDataStream & operator >> (QDataStream &arch, Couple & object)
    {
        arch >> object.ligne;
        arch >> object.depart;
        arch >> object.arrivee;
        return arch;
    }

public:
    /*!
         *  \brief Constructeur
         *
         *  Constructeur par defaut de la classe Couple
         */
    Couple() = default;
    /*!
         *  \brief Constructeur
         *
         *  Constructeur de la classe Couple
         *
         *  \param ligne : ligne a laquelle appartient le couple
         */
    Couple(const Ligne & ligne);
    /*!
         *  \brief Getter de la station de depart du couple
         *  \return La station de depart du couple
         */
    Station getDepart() const;
    /*!
         *  \brief Setter de la station de depart du couple
         *
         *  \param depart : station de depart a definir dans le couple
         */
    void setDepart(const Station & depart);
    /*!
         *  \brief Getter de la station d'arrivee du couple
         *  \return La station de d'arrivee du couple
         */
    Station getArrivee() const;
    /*!
         *  \brief Setter de la station d'arrivee du couple
         *
         *  \param arrivee : station de depart a definir dans le couple
         */
    void setArrivee(const Station & arrivee);
    /*!
         *  \brief Getter de la ligne du couple
         *
         *  \return La ligne du couple
         */
    Ligne getLigne() const;
    /*!
         *  \brief Setter de la ligne du couple
         *
         *  \param ligne : ligne a definir pour le couple
         */
    void setLigne(const Ligne & ligne);

private:
    Ligne ligne; /*!< Ligne de transports en commun associee au couple*/
    Station depart; /*!< Station de depart du couple*/
    Station arrivee; /*!< Station d'arrivee du couple*/
};

Q_DECLARE_METATYPE(Couple)

#endif // COUPLEMODEL_H
