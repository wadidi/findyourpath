#ifndef LIGNELISTMODEL_H
#define LIGNELISTMODEL_H

/*!
 * \file lignelistmodel.h
 * \brief Modele des lignes de transports en commun
 * \author Lucas Miallet & Mehdi Khadir
 */

#include <QAbstractListModel>
#include <QList>
#include <QObject>
#include <QModelIndex>
#include <QVariant>

#include "ligne.h"

/*! \class LigneListModel
 * \brief Modele des lignes de transport en commun
 *
 *  Herite de QAbstractListModel
 */

class LigneListModel : public QAbstractListModel
{

public:

    const static int GET_LIGNE = 123;

    /*!
         *  \brief Constructeur
         *
         *  Constructeur de la classe LigneListModel
         *
         *  \param listeLignes : liste des lignes remplissant le modele
         *  \param parent : QObject parent, 0 par defaut
         */

    LigneListModel(const QList<Ligne> & listeLignes, QObject * parent = 0);

    /*!
         *  \brief Nombre de lignes
         *  \return Nombre de lignes dans le modele
         */

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    /*!
         *  \brief Getter d'une ligne
         *  \param index : Index dans le modele de la ligne que l'on souhaite obtenir
         *  \param role : maniere de retourner la ligne
         *  \return Ligne a l'index index
         */

    QVariant data(const QModelIndex &index, int role) const;

private:
    QList<Ligne> listeLignes; /*!< Liste de lignes du modele*/
};

#endif // LIGNELISTMODEL_H
