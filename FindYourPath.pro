#-------------------------------------------------
#
# Project created by QtCreator 2017-02-28T15:12:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FindYourPath
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    trajetlistmodel.cpp \
    lignelistmodel.cpp \
    couple.cpp \
    station.cpp \
    trajet.cpp \
    qeditablevalue.cpp \
    renderarea.cpp \
    ligne.cpp \
    settinghandler.cpp \
    lineinfos.cpp \
    stationlistmodel.cpp \
    pointdrawer.cpp
    lineinfos.cpp

HEADERS  += mainwindow.h \
    ligne.h \
    trajetlistmodel.h \
    lignelistmodel.h \
    couple.h \
    station.h \
    trajet.h \
    qeditablevalue.h \
    renderarea.h \
    settinghandler.h \
    lineinfos.h \
    stationlistmodel.h \
    pointdrawer.h
    lineinfos.h

FORMS    += mainwindow.ui \
    qeditablevalue.ui

RESOURCES += \
    FYPResources.qrc

SUBDIRS += \
    FindYourPath_BACKUP_2782.pro \
    FindYourPath_BASE_2782.pro \
    FindYourPath_LOCAL_2782.pro \
    FindYourPath_REMOTE_2782.pro \
    FindYourPath_BACKUP_2782.pro \
    FindYourPath_BASE_2782.pro \
    FindYourPath_LOCAL_2782.pro \
    FindYourPath_REMOTE_2782.pro

DISTFILES += \
    FindYourPath.pro.user \
    FindYourPath.pro.user.15bf419 \
    FindYourPath.pro.user.e1a11bc \
    README.md
