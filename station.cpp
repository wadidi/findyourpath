#include "station.h"

Station::Station(const QString & nom) : nom(nom)
{}

Station::Station() : nom("Nom Station"){}

QString Station::getNom() const {
    return nom;
}

void Station::setNom(const QString & nom){
    this->nom = nom;
}

bool Station::operator==(const Station & s){
    return this->nom == s.getNom();
}
