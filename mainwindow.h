#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*!
 * \file mainwindow.h
 * \brief Fenetre principale du programme
 * \author Lucas Miallet & Mehdi Khadir
 */

#include <QMainWindow>
#include <QModelIndex>
#include <QColor>

#include "qeditablevalue.h"
#include "trajet.h"

/*! \namespace Ui
 *
 * espace de nommage regroupant les elements composant la fenetre principale
 */

namespace Ui {
class MainWindow;
}

/*! \class MainWindow
 * \brief classe representant la fenetre principale
 */

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    /*!
     *  \brief Constructeur
     *
     *  Constructeur de la classe MainWindow
     *
     *  \param parent : le QWidget parent de la MainWindow, vaut 0 par defaut
     */

    explicit MainWindow(QWidget *parent = 0);

    /*!
     *  \brief Destructeur
     *
     *  Destructeur de la classe MainWindow
     */

    ~MainWindow();

    /*!
     *  \brief Ajout d'une ligne
     *
     *  Ajoute une ligne de couleur color au dessin representant le trajet. Ce trait represente une portion du trajet
     *  entre une station de depart et une station d'arrivee.
     *
     *  \param color : couleur de la ligne
     */

    void addLine(const QColor & color);

    /*!
     *  \brief Ajout de station dans la liste de stations du trajet
     *
     *  Ajoute une station de la ligne ligne dans la qlistwidget de stations sur la droite de la fenetre principale.
     *  Si la station ajoutee est la premiere station, son nom doit etre place en troisieme position de la fonction.
     *  Le premier parametre de la fonction est toujours une station de correspondance ou d'arrivee.
     *  Connecte les stations rajoutees avec le SLOT on_stationChanged_triggered()
     *
     *  \param station : station d'arrivee ou de correspondance a ajouter
     *  \param ligne : ligne a laquelle correspond la (ou les) station(s) a ajouter
     *  \param firstStation : station de depart du trajet si cette station n'est pas deja presente. Le titre de cette station est "Nom de la station" par defaut
     */

    void addStationListe(QEditableValue * station, const Ligne & ligne, const QString & firstStation = QString("Nom de la station"));
    void addStationListe(QList<QEditableValue*> stations);

    /*!
     *  \brief Definition de la fonction de clic sur un trajet
     *
     *  Lorsqu'on clique sur un trajet, cette fonction est appelee. Elle met a jour l'ensemble des informations affichees
     *  concernant le trajet clique.
     *
     *  \param item : element clique
     */
    void clickSavedTrajet(QModelIndex item);

public slots:

    /*!
     *  \brief SLOT appele lors du clic sur une ligne
     *
     *  Declenche le dessin d'une nouvelle branche sur le trajet, et ajoute au moins une station dans la liste de
     *  stations à droite de la fenetre. En ajoute 2 si aucune station n'a encore ete rajoutee.
     *
     *  \param item : numero de ligne dans la liste de Ligne en bas
     */

    void onClickNewLigne(QModelIndex item);

    /*!
     *  \brief SLOT appele lors du clic sur un trajet
     *
     *  Nettoye la zone de dessin actuelle, et appelle les fonctions de dessin pour representer le trajet
     *  a l'index item dans la liste de trajets sur la gauche de la fenetre principale
     *
     *  \param item : numero de ligne dans la liste de trajets de gauche
     */

    void onClickSavedTrajet(QModelIndex item);

    /*!
     *  \brief SLOT appele lors du changement de nom du trajet ouvert
     *
     *  Sauvegarde le nouveau nom name du trajet
     *
     *  \param name : nouveau nom du trajet rentre par l'utilisateur dans le qeditablevalue
     */

    void changeNameTrajet(QVariant name);

    /*!
     *  \brief SLOT appele lors du changement de commentaire du trajet ouvert
     *
     *  Sauvegarde le nouveau commentaire associe au trajet
     *
     *  \param comment : nouveau commentaire du trajet rentre par l'utilisateur dans le QLineEdit
     */

    void changeComment(QString comment);

private slots:

    /*!
     *  \brief SLOT appele lors du clic sur "Sauvegarder trajet"
     *
     *  Enregistre le trajet courant et nettoye la zone de dessin
     */

    void on_actionEnregistrer_triggered();

    /*!
     *  \brief SLOT appele lors du clic sur "Nouveau trajet"
     *
     *  Nettoye la zone de dessin actuelle, et cree un nouveau trajet
     */

    void on_newTrajet_triggered();

    /*!
     *  \brief SLOT appele lorsqu'une station change de nom
     *
     *  Met à jour le trajet actualTrajet interne avec les nouvelles valeurs rentrees par l'utilisateur dans la liste de
     *  stations de droite
     */

    void on_stationChanged_triggered();

    /*!
     *  \brief SLOT appele lorsque l'utilisateur clique sur "Supprimer station"
     *
     *  Supprime la derniere station rajoutee, ou les 2 denieres stations s'il n'y en a que 2
     */

    void on_actionSupprimer_station_triggered();

    void on_actionSupprimer_tous_les_trajets_triggered();

    void on_actionSupprimer_trajet_triggered();

private:
    Ui::MainWindow *ui;
    int nbStations = 0; /*!< Nombre de stations dessinees*/
    Trajet actualTrajet; /*!< Trajet actuellement ouvert*/
    int actualIndexTrajet = 0;
    bool isSaved = false; /*!< Etat de sauvegarde*/
    bool isModified = false; /*!< Etat de modification*/
};

#endif // MAINWINDOW_H
