#include <QDebug>
#include <QPainter>
#include <QBitmap>
#include <QAbstractItemDelegate>
#include <QScrollBar>
#include <QValidator>
#include <QLineEdit>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "station.h"
#include "ligne.h"
#include "trajetlistmodel.h"
#include "couple.h"
#include "trajet.h"
#include "lignelistmodel.h"
#include "qeditablevalue.h"
#include "renderarea.h"
#include "lineinfos.h"
#include "settinghandler.h"
#include "pointdrawer.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Liste des lignes et de leurs couleurs et noms
    LineInfos lineInfos;
    QList<Ligne> listeLignes(lineInfos.getList());

    QList<Couple> listeCouples;
    listeCouples << Couple(*listeLignes.begin());

    LigneListModel * listeLignesL = new LigneListModel(listeLignes);
    ui->listeLigne->setModel(listeLignesL);
    ui->listeLigne->setViewMode(QListView::IconMode);
    ui->listeLigne->setIconSize(QSize(ui->listeLigne->size().height()*2-10, ui->listeLigne->size().height()*2-10));

    //Trajet list
    TrajetListModel * listeTrajets = new TrajetListModel(SettingHandler::sharedInstance()->getAllTrajet());
    ui->listeTrajets->setModel(listeTrajets);

    //Titre du trajet editable
    /*QEditableValue * trajetNameEdit(new QEditableValue(this,"",QEditableValue::STRING,
                                                       QVariant::fromValue<QString>("Nouveau Trajet")));

    ui->conteneurCreationTrajet->insertWidget(0, trajetNameEdit);*/

    //Nom des stations éditable à droite des dessins de lignes

    ui->listStations->setFrameShape(QFrame::NoFrame);

    QListWidgetItem *stationWidgetItemBlank = new QListWidgetItem;

    ui->listStations->setStyleSheet("QListWidget::item:hover { background: transparent; }");
    ui->drawableArea->setStyleSheet("QListWidget::item:hover { background: transparent; }");


    //NameStationDelegate * nameStationDelegate = new NameStationDelegate();
    //ui->listStations->setItemDelegate(nameStationDelegate);


    //Insertion des dessins de lignes

    ui->drawableArea->setFrameShape(QFrame::NoFrame);

    QListWidgetItem *dessinWidgetItem = new QListWidgetItem;
    QListWidgetItem *dessinWidgetItemBlank = new QListWidgetItem;
    dessinWidgetItemBlank->setFlags(Qt::ItemIsEnabled);


    connect(ui->listeLigne, SIGNAL(clicked(const QModelIndex)), this, SLOT(onClickNewLigne(QModelIndex)));
    connect(ui->listeTrajets, SIGNAL(clicked(const QModelIndex)), this, SLOT(onClickSavedTrajet(QModelIndex)));

    /*Faire scroller la render area lorsqu'on scroll dans la liste de stations et inversement */

    int _currentSliderValue = 0;
    QScrollBar *drawScrollBar = ui->drawableArea->verticalScrollBar();
    QScrollBar *stationsListScrollBar = ui->listStations->verticalScrollBar();
    connect(drawScrollBar, &QAbstractSlider::valueChanged,
                [=](int aSliderPosition)mutable{if(aSliderPosition != _currentSliderValue)
                                         {
                                            stationsListScrollBar->setValue(aSliderPosition);
                                            _currentSliderValue = aSliderPosition;
                                         }
                });

    connect(stationsListScrollBar, &QAbstractSlider::valueChanged,
                [=](int aSliderPosition)mutable{if(aSliderPosition != _currentSliderValue)
                                         {
                                            drawScrollBar->setValue(aSliderPosition);
                                            _currentSliderValue = aSliderPosition;
                                         }
                });


    //S'il y a déjà des trajets enregistres

    if(SettingHandler::sharedInstance()->getAllTrajet().size() > 0) {
        Trajet t = SettingHandler::sharedInstance()->getAllTrajet()[0]; //On prend le premier trajet de la liste des trajets enregistres

        QEditableValue * trajetNameEdit(new QEditableValue(this,"",QEditableValue::STRING,
                                                           QVariant::fromValue<QString>(t.getNom())));

        QLineEdit * comment;
        comment = new QLineEdit(t.getComment());

        //Traitement du validator dans la zone de commentaires
        QRegExp rx("([A-Z]|[a-z]|[ ]|é|è|à|ä|ë|ç|ù|ê|î)*"); //Escape special characters
        QValidator *validator = new QRegExpValidator(rx, this);
        comment->setValidator(validator);

        connect(trajetNameEdit, SIGNAL(valueChanged(const QVariant)), this, SLOT(changeNameTrajet(QVariant)));
        connect(comment, SIGNAL(textEdited(const QString)), this, SLOT(changeComment(QString)));

        ui->conteneurCreationTrajet->insertWidget(0, trajetNameEdit);
        ui->conteneurCreationTrajet->addWidget(comment); //Ajout du widget de commentaire

        // Dessin du trajet t
        ui->drawableArea->clear();
        ui->listStations->clear();
        nbStations = 0;
        for(int i=0; i<t.size(); i++){
            addLine(t.getCouple()[i].getLigne().getColor());
            QEditableValue * station(new QEditableValue(this,"",QEditableValue::STRING,
                                                               QVariant::fromValue<QString>(t.getCouple()[i].getArrivee().getNom())));
            if(i == 0)
                addStationListe(station, t.getCouple()[i].getLigne(),t.getCouple()[i].getDepart().getNom());
            else
                addStationListe(station, t.getCouple()[i].getLigne());
        }

        actualTrajet.setNom(t.getNom());
        actualTrajet.setComment(t.getComment());
    } else {
        QEditableValue * trajetNameEdit(new QEditableValue(this,"",QEditableValue::STRING,
                                                               QVariant::fromValue<QString>("Nouveau Trajet")));

        ui->conteneurCreationTrajet->insertWidget(0, trajetNameEdit);

        QLineEdit * comment;
        comment = new QLineEdit("Commentaire");

        //Traitement du validator dans la zone de commentaires
        QRegExp rx("([A-Z]|[a-z]|[ ]|é|è|à|ä|ë|ç|ù|ê|î)*"); //Escape special characters
        QValidator *validator = new QRegExpValidator(rx, this);
        comment->setValidator(validator);
    }

}

MainWindow::~MainWindow()
{
    delete ui;

}

/*
 * Ajout d'une nouvelle ligne dans la render area
 */
void MainWindow::onClickNewLigne(QModelIndex item) {
    Ligne ll = ui->listeLigne->model()->data(item, 123).value<Ligne>();
    addLine(ll.getColor());
    QEditableValue * station(new QEditableValue(0,"",QEditableValue::STRING,
                                                 QVariant::fromValue<QString>("Nom de la station")));
    addStationListe(station, ll);

    //Etats de sauvegarde et de modification
    this->isModified = true;
}

/*
 * Récupération d'un des trajets déjà sauvegardé et l'affiche
 */
void MainWindow::onClickSavedTrajet(QModelIndex item) {
    clickSavedTrajet(item);
}

/*
 * Enregistrer le trajet défini
 */
void MainWindow::on_actionEnregistrer_triggered()
{
    SettingHandler::sharedInstance()->saveTrajet(actualTrajet, this->isSaved, this->actualIndexTrajet);
    ((TrajetListModel *)ui->listeTrajets->model())->updateRow(actualTrajet, actualIndexTrajet);
    this->isSaved = true;
    this->isModified = false;
}

void MainWindow::changeNameTrajet(QVariant name) {
    ((TrajetListModel *)ui->listeTrajets->model())->changeName(name.value<QString>(), this->actualIndexTrajet);
    actualTrajet.setNom(name.value<QString>());
    this->isModified = true;
}

void MainWindow::changeComment(QString comment){
    ((TrajetListModel *)ui->listeTrajets->model())->changeComment(comment, this->actualIndexTrajet);
    actualTrajet.setComment(comment);
    this->isModified = true;
}

/*
 * Création d'un nouveau trajet
 */
void MainWindow::on_newTrajet_triggered()
{
    nbStations = 0; //Nombre de stations dessinées remis à 0
    ui->drawableArea->clear();
    ui->listStations->clear();

    /*
     * C'est pas très beau mais j'ai essayé de changer dynamiquement le qeditablevalue déjà existant
     * ça ne fonctionnait pas du coup je supprime l'objet et je le recrée...
     */
    delete ui->conteneurCreationTrajet->itemAt(0)->widget();
    QEditableValue * trajetNameEdit(new QEditableValue(this,"",QEditableValue::STRING,
                                                       QVariant::fromValue<QString>("Nouveau trajet")));
    int nbEltsCont = ui->conteneurCreationTrajet->count();
    delete ui->conteneurCreationTrajet->itemAt(nbEltsCont-1)->widget();
    QLineEdit * comment;
    comment = new QLineEdit("Commentaire");
    //Traitement du validator dans la zone de commentaires
    QRegExp rx("([A-Z]|[a-z]|[ ]|é|è|à|ä|ë|ç|ù|ê|î)*"); //Escape special characters
    QValidator *validator = new QRegExpValidator(rx, this);
    comment->setValidator(validator);

    connect(trajetNameEdit, SIGNAL(valueChanged(const QVariant)), this, SLOT(changeNameTrajet(QVariant)));
    connect(comment, SIGNAL(textEdited(const QString)), this, SLOT(changeComment(QString)));

    ui->conteneurCreationTrajet->insertWidget(0, trajetNameEdit);
    ui->conteneurCreationTrajet->addWidget(comment);

    Trajet* monTrajet = new Trajet("Nouveau Trajet");
    ((TrajetListModel *)ui->listeTrajets->model())->insertNewTrajet(monTrajet, 0);
    this->actualIndexTrajet = 0;
    this->actualTrajet.reset();
    this->isSaved = false;
    this->isModified = true;
}

/*
 * Mise à jour de la liste de couples interne
 */

void MainWindow::on_stationChanged_triggered()
{
    QList<Station> listeProvisoire; //Liste contenant les stations après modification
    for(int i = 0; i < (ui->listStations->count()/2)+1; i++)
    {
        QListWidgetItem * item = ui->listStations->item(i*2); //i*2 pour prendre en compte les blancs dans la liste de stations
        QWidget* widgetCourant = ui->listStations->itemWidget(item);
        QEditableValue* editableCourant = dynamic_cast<QEditableValue*>(widgetCourant);
        QString str = editableCourant->getValue().toString();
        listeProvisoire<<Station(str); //Ajout de la station dans la liste provisoire
    }

    for(int j = 0; j<listeProvisoire.length()-1; j++){
        actualTrajet.setCouple(listeProvisoire.at(j),listeProvisoire.at(j+1),j);
    }

    //Etats de sauvegarde et de modification
    this->isModified = true;
}

/*Fonction d'ajout graphique d'une ligne de métro
 */

void MainWindow::addLine(const QColor &color){
    if(nbStations == 0){//S'il n'y a pas de station, on rajoute un point
        PointDrawer * dessinLigne2 = new PointDrawer(color);
        QListWidgetItem *dessinWidgetItem2 = new QListWidgetItem;
        dessinWidgetItem2->setSizeHint(QSize(100,80));
        ui->drawableArea->addItem(dessinWidgetItem2);
        ui->drawableArea->setItemWidget(dessinWidgetItem2, dessinLigne2);
    }
    RenderArea * dessinLigne = new RenderArea(color);
    QListWidgetItem *dessinWidgetItem = new QListWidgetItem;
    dessinWidgetItem->setSizeHint(QSize(100,307.5));
    ui->drawableArea->addItem(dessinWidgetItem);
    ui->drawableArea->setItemWidget(dessinWidgetItem, dessinLigne);

    //Etats de sauvegarde et de modification
    this->isModified = true;

}

/*Fonction d'ajout de la liste de stations sur la droite
 */

void MainWindow::addStationListe(QEditableValue * station, const Ligne & ligne, const QString & firstStationParam){
    QListWidgetItem *stationWidgetItem = new QListWidgetItem;
    stationWidgetItem->setSizeHint(QSize(10,50));

    //Blank space between the station names
    QListWidgetItem *stationWidgetItemBlank = new QListWidgetItem;
    stationWidgetItemBlank->setSizeHint(QSize(10,257.5));
    stationWidgetItemBlank->setFlags(Qt::ItemIsEnabled);

    //Mise à jour du trajet
    Couple couple;

    if(nbStations == 0){ //Ajout de 2 stations blanches
        QEditableValue * firstStation(new QEditableValue(0,"",QEditableValue::STRING,
                                                     QVariant::fromValue<QString>(firstStationParam)));
        QListWidgetItem *stationWidgetItem2 = new QListWidgetItem;
        stationWidgetItem2->setSizeHint(QSize(10,80));
        ui->listStations->addItem(stationWidgetItem2);
        ui->listStations->setItemWidget(stationWidgetItem2, firstStation);
        nbStations++;
        //Enregistrement de la station de départ dans le couple
        Station depart(firstStationParam);
        couple.setDepart(depart);
        connect(firstStation, SIGNAL(valueChanged(QVariant)), this, SLOT(on_stationChanged_triggered()));
    }

    //Ajout des stations dans la liste
    ui->listStations->addItem(stationWidgetItemBlank);
    ui->listStations->addItem(stationWidgetItem);
    ui->listStations->setItemWidget(stationWidgetItem, station);
    connect(station, SIGNAL(valueChanged(QVariant)), this, SLOT(on_stationChanged_triggered()));

    //Mise à jour de l'arrivée du couple
    Station arrivee(station->getValue().toString());
    couple.setArrivee(arrivee);

    //Definition de la ligne
    couple.setLigne(ligne);

    //Ajout du nouveau couple dans le trajet
    actualTrajet.addCouple(couple);

    nbStations++;

    //Etats de sauvegarde et de modification
    this->isModified = true;
}

void MainWindow::addStationListe(QList<QEditableValue*> stations){
    QListWidgetItem *stationWidgetItem = new QListWidgetItem;
    stationWidgetItem->setSizeHint(QSize(10,50));

    //Ajout de la première station dans la liste
    ui->listStations->addItem(stationWidgetItem);
    ui->listStations->setItemWidget(stationWidgetItem, stations.at(0));


    //Ajout des autres stations
    for(QList<QEditableValue*>::iterator it = stations.begin()+1; it != stations.end(); ++it){

        //Blank space between the station names
        QListWidgetItem *stationWidgetItemBlank = new QListWidgetItem;
        stationWidgetItemBlank->setSizeHint(QSize(10,240));
        stationWidgetItemBlank->setFlags(Qt::ItemIsEnabled);
        ui->listStations->addItem(stationWidgetItemBlank);

        //Ajout de la station traitée
        QListWidgetItem *stationWidgetItem = new QListWidgetItem;
        stationWidgetItem->setSizeHint(QSize(10,50));
        ui->listStations->addItem(stationWidgetItem);
        ui->listStations->setItemWidget(stationWidgetItem, *it);
    }
}

void MainWindow::on_actionSupprimer_station_triggered()
{
    //Cas où il n'y a aucune station, on ne fait rien
    if(nbStations == 0){
        qDebug()<<"Hello ici";
        return;
    }
    //Cas où il n'y a que 2 stations
    if(nbStations == 2){
        nbStations = 0;
        actualTrajet.reset(); //actualTrajet vide
        ui->drawableArea->clear(); //Nettoyage de la zone de dessin
        ui->listStations->clear();
    }
    else{
        qDebug()<< "hello là";
        //Suppression des derniers QListWidgetItem
        ui->listStations->takeItem(ui->listStations->count()-1);
        ui->listStations->takeItem(ui->listStations->count()-1);
        ui->drawableArea->takeItem(ui->drawableArea->count()-1); //Suppression du blanc entre les noms de stations à droite
        actualTrajet.removeLastCouple();
        nbStations--;
    }

    //Etats de sauvegarde et de modification
    this->isModified = true;
}

void MainWindow::on_actionSupprimer_tous_les_trajets_triggered()
{
    SettingHandler::sharedInstance()->deleteAll();
    ((TrajetListModel *)ui->listeTrajets->model())->clear();

    nbStations = 0; //Nombre de stations dessinées remis à 0
    ui->drawableArea->clear();
    ui->listStations->clear();

    delete ui->conteneurCreationTrajet->itemAt(0)->widget();
    QEditableValue * trajetNameEdit(new QEditableValue(this,"",QEditableValue::STRING,
                                                       QVariant::fromValue<QString>("Nouveau trajet")));

    int nbEltsCont = ui->conteneurCreationTrajet->count();
    delete ui->conteneurCreationTrajet->itemAt(nbEltsCont-1)->widget();
    QLineEdit * comment;
    comment = new QLineEdit("Commentaire");
    //Traitement du validator dans la zone de commentaires
    QRegExp rx("([A-Z]|[a-z]|[ ]|é|è|à|ä|ë|ç|ù|ê|î)*"); //Escape special characters
    QValidator *validator = new QRegExpValidator(rx, this);
    comment->setValidator(validator);


    connect(trajetNameEdit, SIGNAL(valueChanged(const QVariant)), this, SLOT(changeNameTrajet(QVariant)));
    connect(comment, SIGNAL(textEdited(const QString)), this, SLOT(changeComment(QString)));


    ui->conteneurCreationTrajet->insertWidget(0, trajetNameEdit);
    ui->conteneurCreationTrajet->addWidget(comment);

    Trajet* monTrajet = new Trajet("Nouveau Trajet");
    ((TrajetListModel *)ui->listeTrajets->model())->insertNewTrajet(monTrajet, 0);
    this->actualIndexTrajet = 0;
    this->actualTrajet.reset();
    this->isSaved = false;
    this->isModified = true;

}

void MainWindow::on_actionSupprimer_trajet_triggered()
{
    this->actualTrajet.reset(); //Reinitialisation du trajet actuellement montre dans la fenetre

    SettingHandler::sharedInstance()->deleteTrajet(actualIndexTrajet); //Suppression du trajet dans les qsettings
    ((TrajetListModel *)ui->listeTrajets->model())->deleteTrajet(actualIndexTrajet); //Suppression du trajet dans le modele

    if(actualIndexTrajet == ui->listeTrajets->model()->rowCount()){    //Si c'est le dernier element de la liste de trajets
        actualIndexTrajet--;
    }

    QModelIndex item = ui->listeTrajets->model()->index(actualIndexTrajet,0);
    Trajet t;
    t = ui->listeTrajets->model()->data(item, TrajetListModel::GET_TRAJET).value<Trajet>();
    actualTrajet = t;
    clickSavedTrajet(item); //Emission d'un clic sur l'element courant dans la qlistview de trajets afin de mettre a jour les informations
}

void MainWindow::clickSavedTrajet(QModelIndex item){

    Trajet t;
    if(this->actualIndexTrajet == item.row()) {
        qDebug() << "Same index";
        t = actualTrajet;

    } else {
        qDebug() << "Different index";
        this->actualIndexTrajet = item.row();
        t = ui->listeTrajets->model()->data(item, TrajetListModel::GET_TRAJET).value<Trajet>();
    }


    delete ui->conteneurCreationTrajet->itemAt(0)->widget();
    QEditableValue * trajetNameEdit(new QEditableValue(this,"",QEditableValue::STRING,
                                                       QVariant::fromValue<QString>(t.getNom())));

    int nbEltsCont = ui->conteneurCreationTrajet->count();
    delete ui->conteneurCreationTrajet->itemAt(nbEltsCont-1)->widget();
    QLineEdit * comment;
    comment = new QLineEdit(t.getComment());
    //Traitement du validator dans la zone de commentaires
    QRegExp rx("([A-Z]|[a-z]|[ ]|é|è|à|ä|ë|ç|ù|ê|î)*"); //Escape special characters
    QValidator *validator = new QRegExpValidator(rx, this);
    comment->setValidator(validator);

    connect(trajetNameEdit, SIGNAL(valueChanged(const QVariant)), this, SLOT(changeNameTrajet(QVariant)));
    connect(comment, SIGNAL(textEdited(const QString)), this, SLOT(changeComment(QString)));

    ui->conteneurCreationTrajet->insertWidget(0, trajetNameEdit);
    ui->conteneurCreationTrajet->addWidget(comment);

    //qDebug() << t.getNom();
    /*if (t.getCouple().size() > 0) {
        qDebug() << t.getCouple()[0].getDepart().getNom();
    }*/
    if(!this->isModified) {
        this->isSaved = true;
    }

    //Dessin du trajet t
    ui->drawableArea->clear();
    ui->listStations->clear();
    nbStations = 0;
    actualTrajet.reset();
    actualTrajet.setNom(t.getNom());
    actualTrajet.setComment(t.getComment());
    for(int i=0; i<t.size(); i++){
        addLine(t.getCouple()[i].getLigne().getColor());
        QEditableValue * station(new QEditableValue(this,"",QEditableValue::STRING,
                                                           QVariant::fromValue<QString>(t.getCouple()[i].getArrivee().getNom())));
        if(i == 0)
            addStationListe(station, t.getCouple()[i].getLigne(),t.getCouple()[i].getDepart().getNom());
        else
            addStationListe(station, t.getCouple()[i].getLigne());
    }
}
