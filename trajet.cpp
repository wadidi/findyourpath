#include "trajet.h"

Trajet::Trajet(const QString & nom) : nom(nom){}

Trajet::Trajet(const Trajet & t) {
    this->comments = t.getComment();
    this->nom = t.getNom();
    this->trajet = t.trajet;
}

QString Trajet::getNom() const {
    return nom;
}

void Trajet::setNom(QString name) {
    this->nom = name;
}

const QList<Couple> Trajet::getCouple() const {
    return this->trajet;
}

void Trajet::addCouple(Couple & couple){
    trajet << couple;
}

QString Trajet::toString(){
    QString result;
    QListIterator<Couple> iterator(trajet);
    while(iterator.hasNext()){
        Couple current = iterator.next();
        result.append("Départ : ").append(current.getDepart().getNom());
        result.append("Arrivee : ").append(current.getArrivee().getNom());
    }
    return result;
}

void Trajet::reset(){
    trajet.clear();
    nom = "Nouveau Trajet";
    comments = "Commentaire";
}

void Trajet::setCouple(Station depart, Station arrivee, int index){
    Ligne l = trajet.value(index).getLigne(); //Ligne gardee en memoire pour le remplacement
    Couple couple(l); //Creation du couple de remplacement
    couple.setDepart(depart);
    couple.setArrivee(arrivee);
    trajet.replace(index,couple);
}

int Trajet::size(){
    return trajet.size();
}

void Trajet::removeLastCouple(){
    trajet.removeLast();
}

void Trajet::setComment(QString comment){
    this->comments = comment;
}

QString Trajet::getComment() const{
    return comments;
}
