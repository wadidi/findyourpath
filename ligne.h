#ifndef LIGNEMODEL_H
#define LIGNEMODEL_H

/*!
 * \file ligne.h
 * \brief Informations sur une ligne de transports en commun
 * \author Lucas Miallet & Mehdi Khadir
 */

#include <QColor>
#include <QString>
#include <QList>
#include <QIcon>
#include "station.h"

  /*! \class Ligne
   * \brief Informations sur une ligne de transports en commun
   *
   *  Contient toutes les informations d'une ligne de transports en commun necessaires au bon fonctionnement de l'application
   */

class Ligne
{

    friend QDataStream & operator << (QDataStream &arch, const Ligne & object)
    {
        arch << object.nom;
        arch << object.color;
        arch << object.icon;
        return arch;
    }

    friend QDataStream & operator >> (QDataStream &arch, Ligne & object)
    {
        arch >> object.nom;
        arch >> object.color;
        arch >> object.icon;
        return arch;
    }

public:
    /*!
         *  \brief Constructeur
         *
         *  Constructeur par defaut de la classe Ligne
         */
    Ligne() = default;
    /*!
         *  \brief Constructeur
         *
         *  Constructeur de la classe Ligne
         *
         *  \param color : Couleur de la ligne de transports en commun
         *  \param nom : Nom de la ligne de transports en commun
         */
    Ligne(QColor color, QString nom);
    /*!
         *  \brief Constructeur
         *
         *  Constructeur de la classe Ligne
         *
         *  \param color : Couleur de la ligne de transports en commun
         *  \param nom : Nom de la ligne de transports en commun
         *  \param icon : Icone representatif de la ligne de transports en commun
         */
    Ligne(QColor color, QString nom, QIcon icon);
    /*!
         *  \brief Getter de la couleur de la ligne
         *  \return La couleur de la ligne
         */
    QColor getColor() const;
    /*!
         *  \brief Getter du nom de la ligne
         *  \return Le nom de la ligne
         */
    QString getNom() const;
    /*!
         *  \brief Ajout d'une station
         *
         *  Methode qui permet d'ajouter la Station station a la liste de stations de la ligne
         *
         *  \param station : la station a rajouter
         */
    void addStation(const Station & station);
    /*!
         *  \brief Suppression d'une station
         *
         *  Methode qui permet de supprimer de la ligne la station dont le nom est nom
         *
         *  \param nom : nom de la station a supprimer
         *  \return La station supprimee
         */
    Station removeStation(const QString & nom);
    /*!
         *  \brief Getter de l'icone
         *
         *  Renvoie l'icone associe
         *  a la ligne de transports en commun
         *
         *  \return icone de la ligne
         */
    QIcon getIcon() const;

private:
    QString nom; /*!< Nom de la ligne*/
    QColor color; /*!< Couleur de la ligne*/
    QList<Station> stations; /*!< Liste des stations dont la ligne est constituee*/
    QIcon icon; /*!< Icone associe a la ligne*/
};

Q_DECLARE_METATYPE(Ligne)

#endif // LIGNEMODEL_H
