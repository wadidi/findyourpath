#include "lineinfos.h"

LineInfos::LineInfos()
{
    //Enregistrement de chaque ligne de métro l'une après l'autre

    lignesList << Ligne(QColor(255,205,0),"1",QIcon(":/logoMetro/metroLogosPng/metrol1.png"));
    lignesList << Ligne(QColor(0,60,166),"2",QIcon(":/logoMetro/metroLogosPng/metrol2.png"));
    lignesList << Ligne(QColor(131,121,2),"3",QIcon(":/logoMetro/metroLogosPng/metrol3.png"));
    lignesList << Ligne(QColor(110,196,232),"3 bis",QIcon(":/logoMetro/metroLogosPng/metrol3b.png"));
    lignesList << Ligne(QColor(207,0,158),"4",QIcon(":/logoMetro/metroLogosPng/metrol4.png"));
    lignesList << Ligne(QColor(255,126,46),"5",QIcon(":/logoMetro/metroLogosPng/metrol5.png"));
    lignesList << Ligne(QColor(110,202,151),"6",QIcon(":/logoMetro/metroLogosPng/metrol6.png"));
    lignesList << Ligne(QColor(250,154,186),"7",QIcon(":/logoMetro/metroLogosPng/metrol7.png"));
    lignesList << Ligne(QColor(110,202,151),"7 bis",QIcon(":/logoMetro/metroLogosPng/metrol7b.png"));
    lignesList << Ligne(QColor(225,155,223),"8",QIcon(":/logoMetro/metroLogosPng/metrol8.png"));
    lignesList << Ligne(QColor(182,189,0),"9",QIcon(":/logoMetro/metroLogosPng/metrol9.png"));
    lignesList << Ligne(QColor(201,145,13),"10",QIcon(":/logoMetro/metroLogosPng/metrol10.png"));
    lignesList << Ligne(QColor(112,75,28),"11",QIcon(":/logoMetro/metroLogosPng/metrol11.png"));
    lignesList << Ligne(QColor(0,120,82),"12",QIcon(":/logoMetro/metroLogosPng/metrol12.png"));
    lignesList << Ligne(QColor(110,196,232),"13",QIcon(":/logoMetro/metroLogosPng/metrol13.png"));
    lignesList << Ligne(QColor(98,37,157),"14",QIcon(":/logoMetro/metroLogosPng/metrol14.png"));

}

Ligne LineInfos::getStation(int index){
    return lignesList.at(index);
}

QList<Ligne> LineInfos::getList(){
    return lignesList;
}
